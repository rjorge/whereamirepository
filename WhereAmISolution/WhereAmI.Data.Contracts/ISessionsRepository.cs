﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WhereAmI.Model;

namespace WhereAmI.Data.Contracts
{
    public interface ISessionsRepository : IRepository<Session>
    {
        Session StartSessionOnUser(ApplicationUser user);
        Session EndSession(int sessionId);
        List<Session> GetSessionsFromUser(ApplicationUser user);
    }
}
