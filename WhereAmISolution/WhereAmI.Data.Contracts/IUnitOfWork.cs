﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WhereAmI.Model;

namespace WhereAmI.Data.Contracts
{
    public interface IUnitOfWork
    {
        void Commit();

        IApplicationUserRepository Users { get; } 
        ISessionsRepository Sessions { get; }
        ILocationPointsRepository LocationPoints { get; }
    }
}