﻿using System.Collections.Generic;
using WhereAmI.Model;

namespace WhereAmI.Data.Contracts
{
    public interface ILocationPointsRepository
    {
        bool UpdateLocation(string longitude, string altitude, int sessionId);
        List<LocationPoint> GetLocationPointsBySession(int sessionId);
    }
}
