﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WhereAmI.Data;
using WhereAmI.Data.Contracts;

namespace WhereAmI.Web.Controllers
{
    public class BaseApiController : ApiController
    {
        public IUnitOfWork Uow { get; set; }
    }
}
