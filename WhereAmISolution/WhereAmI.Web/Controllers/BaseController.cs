﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WhereAmI.Data;
using WhereAmI.Data.Contracts;

namespace WhereAmI.Web.Controllers
{
    public class BaseController : Controller
    {
        public IUnitOfWork Uow { get; set; }
    }
}