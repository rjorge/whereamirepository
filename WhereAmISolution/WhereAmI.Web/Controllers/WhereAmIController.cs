﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WhereAmI.Data;
using WhereAmI.Data.Contracts;
using WhereAmI.Model;

namespace WhereAmI.Web.Controllers
{
    public class WhereAmIController : BaseApiController
    {
        public WhereAmIController(IUnitOfWork uow)
        {
            Uow = uow;
        }

        [HttpPost]
        [ActionName("StartSession")]
        public Session StartSession(string email)
        {
            return Uow.Sessions.StartSessionOnUser(Uow.Users.GetUserByEmail(email));
        }

        [HttpPost]
        [ActionName("EndSession")]
        public Session EndSession(int id)
        {
            return Uow.Sessions.EndSession(id);
        }

        [HttpPost]
        [ActionName("UpdateLocation")]
        public bool UpdateLocation(string longitude, string latitude, int sessionId)
        {
            return Uow.LocationPoints.UpdateLocation(longitude, latitude, sessionId);
        }

        [HttpGet]
        [ActionName("GetSessions")]
        public List<Session> GetSessions(string email)
        {
            return Uow.Sessions.GetSessionsFromUser(Uow.Users.GetUserByEmail(email));
        }

        [HttpGet]
        [ActionName("GetLocationPointsBySession")]
        public List<LocationPoint> GetLocationPointsBySession(int sessionId)
        {
            return Uow.LocationPoints.GetLocationPointsBySession(sessionId);
        }
    }
}
