﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WhereAmI.Web.Startup))]
namespace WhereAmI.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
