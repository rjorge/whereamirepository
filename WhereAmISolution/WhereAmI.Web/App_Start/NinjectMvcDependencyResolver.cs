﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using WhereAmI.Data;
using WhereAmI.Data.Contracts;
using WhereAmI.Data.Helpers;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Ninject;
using Ninject.Modules;
using Ninject.Web.Common;

namespace WhereAmI.Web.App_Start
{
    public class NinjectMvcDependencyResolver : NinjectDependencyScope, IDependencyResolver
    {
        public IKernel Kernel;

        public NinjectMvcDependencyResolver(IKernel kernel) : base(kernel)
        {
            Kernel = kernel;
        }


    }
}