﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;
using WhereAmI.Data;
using WhereAmI.Data.Contracts;
using WhereAmI.Data.Helpers;
using WhereAmI.Web.App_Start;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Ninject;
using Ninject.Web.Common;

namespace WhereAmI.Web
{
    public class IocConfig
    {
        public static void RegisterIoc(HttpConfiguration config)
        {
            var apiKernel = new StandardKernel();
            var mvcKernel = new StandardKernel();
            apiKernel = (StandardKernel) BindKernelApi(apiKernel);
            mvcKernel = (StandardKernel) BindKernelMvc(mvcKernel);

            // Tell WebApi how to use our Ninject IoC
            DependencyResolver.SetResolver(new NinjectMvcDependencyResolver(mvcKernel));
            config.DependencyResolver = new NinjectApiDependencyResolver(apiKernel);
        }

        private static IKernel BindKernelApi(IKernel kernel)
        {
            // These registrations are "per instance request".
            // See http://blog.bobcravens.com/2010/03/ninject-life-cycle-management-or-scoping/

            kernel.Bind<RepositoryFactories>().To<RepositoryFactories>()
                .InSingletonScope();

            kernel.Bind<IRepositoryProvider>().To<RepositoryProvider>();
            kernel.Bind<IUnitOfWork>().To<UnitOfWork>();
            
            return kernel;
        }
        private static IKernel BindKernelMvc(IKernel kernel)
        {
            // These registrations are "per instance request".
            // See http://blog.bobcravens.com/2010/03/ninject-life-cycle-management-or-scoping/

            kernel.Bind<RepositoryFactories>().To<RepositoryFactories>()
                .InRequestScope();

            kernel.Bind<IRepositoryProvider>().To<RepositoryProvider>().InRequestScope();
            kernel.Bind<IUnitOfWork>().To<UnitOfWork>().InRequestScope();

            return kernel;
        }

    }
}