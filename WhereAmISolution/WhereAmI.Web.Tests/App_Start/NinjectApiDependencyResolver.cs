﻿using System.Web.Http.Dependencies;
using Ninject;

namespace WhereAmI.Web
{
    public class NinjectApiDependencyResolver : NinjectDependencyScope, IDependencyResolver
    {
        public IKernel Kernel;

        public NinjectApiDependencyResolver(IKernel kernel)
            : base(kernel)
        {
            this.Kernel = kernel;
        }

        public IDependencyScope BeginScope()
        {
            return new NinjectDependencyScope(Kernel.BeginBlock());
        }
    }
}