namespace WhereAmI.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NullableEndDateOnSessions : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Sessions", "End", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Sessions", "End", c => c.DateTime(nullable: false));
        }
    }
}
