namespace WhereAmI.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Sessions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Start = c.DateTime(nullable: false),
                        End = c.DateTime(nullable: false),
                        ApplicationUser_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUser_Id)
                .Index(t => t.ApplicationUser_Id);
            
            CreateTable(
                "dbo.LocationPoints",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Altitude = c.String(),
                        Latitude = c.String(),
                        Session_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sessions", t => t.Session_Id)
                .Index(t => t.Session_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Sessions", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.LocationPoints", "Session_Id", "dbo.Sessions");
            DropIndex("dbo.LocationPoints", new[] { "Session_Id" });
            DropIndex("dbo.Sessions", new[] { "ApplicationUser_Id" });
            DropTable("dbo.LocationPoints");
            DropTable("dbo.Sessions");
        }
    }
}
