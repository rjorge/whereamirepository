namespace WhereAmI.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModelConventions : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Sessions", name: "ApplicationUser_Id", newName: "User_Id");
            RenameIndex(table: "dbo.Sessions", name: "IX_ApplicationUser_Id", newName: "IX_User_Id");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Sessions", name: "IX_User_Id", newName: "IX_ApplicationUser_Id");
            RenameColumn(table: "dbo.Sessions", name: "User_Id", newName: "ApplicationUser_Id");
        }
    }
}
