namespace WhereAmI.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PropertyNameCorrection : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LocationPoints", "Longitude", c => c.String());
            DropColumn("dbo.LocationPoints", "Altitude");
        }
        
        public override void Down()
        {
            AddColumn("dbo.LocationPoints", "Altitude", c => c.String());
            DropColumn("dbo.LocationPoints", "Longitude");
        }
    }
}
