namespace WhereAmI.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DateOnLocationPoints : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LocationPoints", "InsertedDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.LocationPoints", "InsertedDate");
        }
    }
}
