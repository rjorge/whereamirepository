namespace WhereAmI.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SessionIdForeignKeyOnLocationPoints : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.LocationPoints", "Session_Id", "dbo.Sessions");
            DropIndex("dbo.LocationPoints", new[] { "Session_Id" });
            RenameColumn(table: "dbo.LocationPoints", name: "Session_Id", newName: "SessionId");
            AlterColumn("dbo.LocationPoints", "SessionId", c => c.Int(nullable: false));
            CreateIndex("dbo.LocationPoints", "SessionId");
            AddForeignKey("dbo.LocationPoints", "SessionId", "dbo.Sessions", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LocationPoints", "SessionId", "dbo.Sessions");
            DropIndex("dbo.LocationPoints", new[] { "SessionId" });
            AlterColumn("dbo.LocationPoints", "SessionId", c => c.Int());
            RenameColumn(table: "dbo.LocationPoints", name: "SessionId", newName: "Session_Id");
            CreateIndex("dbo.LocationPoints", "Session_Id");
            AddForeignKey("dbo.LocationPoints", "Session_Id", "dbo.Sessions", "Id");
        }
    }
}
