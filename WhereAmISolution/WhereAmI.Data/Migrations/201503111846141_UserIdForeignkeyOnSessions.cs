namespace WhereAmI.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserIdForeignkeyOnSessions : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Sessions", name: "User_Id", newName: "ApplicationUserId");
            RenameIndex(table: "dbo.Sessions", name: "IX_User_Id", newName: "IX_ApplicationUserId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Sessions", name: "IX_ApplicationUserId", newName: "IX_User_Id");
            RenameColumn(table: "dbo.Sessions", name: "ApplicationUserId", newName: "User_Id");
        }
    }
}
