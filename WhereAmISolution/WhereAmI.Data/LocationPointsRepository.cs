﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WhereAmI.Data.Contracts;
using WhereAmI.Model;

namespace WhereAmI.Data
{
    public class LocationPointsRepository : EFRepository<LocationPoint>, ILocationPointsRepository
    {
        public LocationPointsRepository(DbContext dbContext) : base(dbContext)
        {
        }

        public bool UpdateLocation(string longitude, string latitude, int sessionId)
        {
            var session = DbContext.Set<Session>().First(c => c.Id == sessionId);

            if (session.End != null)
            {
                return false;
            }

            var newLocation = new LocationPoint()
            {
                Longitude = longitude,
                Latitude = latitude,
                SessionId = sessionId,
                InsertedDate = DateTime.Now
                
            };

            DbSet.Add(newLocation);

            try
            {
                DbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public List<LocationPoint> GetLocationPointsBySession(int sessionId)
        {
            return DbSet.Where(c => c.SessionId == sessionId).ToList();
        }
    }
}
