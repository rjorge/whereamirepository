﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WhereAmI.Data.Contracts;
using WhereAmI.Model;

namespace WhereAmI.Data
{
    public class ApplicationUserRepository: EFRepository<ApplicationUser>, IApplicationUserRepository
    {
        public ApplicationUserRepository(DbContext dbContext) : base(dbContext)
        {
        }

        public ApplicationUser GetUserByEmail(string email)
        {
            return DbSet.First(c => c.Email.Equals(email));
        }
    }
}
