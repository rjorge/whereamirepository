﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Ajax.Utilities;
using WebGrease.Css.Extensions;
using WhereAmI.Data.Contracts;
using WhereAmI.Model;

namespace WhereAmI.Data
{
    public class SessionsRepository : EFRepository<Session>, ISessionsRepository
    {
        public SessionsRepository(DbContext dbContext) : base(dbContext)
        {
        }

        public Session StartSessionOnUser(ApplicationUser user)
        {
            var numberOfSessionsStarted = DbSet.Count(c => c.ApplicationUserId.Equals(user.Id) && c.End == null);
            if (numberOfSessionsStarted > 0)
            {
                return null;
            }

            var newSession = new Session()
            {
                ApplicationUserId = user.Id,
                Start = DateTime.Now
            };

            var added = DbSet.Add(newSession);

            try
            {
                DbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                return null;
            }

            return added;
        }

        public Session EndSession(int sessionId)
        {
            var session = DbSet.First(c => c.Id == sessionId);
            session.End = DateTime.Now;

            DbEntityEntry dbEntityEntry = DbContext.Entry(session);
            if (dbEntityEntry.State == EntityState.Detached)
            {
                DbSet.Attach(session);
            }
            dbEntityEntry.State = EntityState.Modified;

            try
            {
                DbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                return null;
            }

            return session;
        }

        public List<Session> GetSessionsFromUser(ApplicationUser user)
        {
            return DbSet.
                Where(c => c.ApplicationUserId == user.Id && c.LocationPoints.Any()).AsEnumerable().
                Select(c => new Session()
            {
                Id = c.Id,
                End = c.End,
                Start = c.Start
            }).ToList();
        }
    }
}
